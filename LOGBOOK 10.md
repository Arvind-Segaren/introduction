## LOGBOOK WEEK 12 (14/1/2022)

## Logbook Entry Number : 10

## Group : Team 1

## Subsystem : Design and Structure

| Week 12 | Subsystem |
|--------|-----------|
| 1. What is the agenda this week and what are my/our goals? | As we planned earlier, the CAD drawing for servo holder has been revised and ready to print for this week. The task is to print the remaining 3 pieces of motor holder and 4 pieces of servo holder. But, the time to print this parts are become constraint for this week. Motor holder will took approximately 4 hours to print each. For servo holder, it will consume 12 hours to print completely for one piece.|
| 2. What decisions did you/your team make in solving a problem? | Since, only one 3D printer is available, I need to work on the time management method carefully in order to print-out all my parts and allow other members to use the 3D printer as well. |
| 3. How did you/your team make those decisions (method)? | I asked other members to print their part in the morning till late noon. After late noon around 3pm and above, I will start to print my part and let it to be print overnight. Perhaps, I will print 2 parts together to boost the printing process. |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Because as per discussed in the discord platform, our thruster arm team has promised Dr. Salahuddin to finish printing process by Wednesday and start to assemble it on Thursady. |
| 5. What was the impact on you/your team/the project when you make that decision? | We were able to finish the printing process for the whole thruster arm in this week. We are happy with the outcome of this process. |
| 6. What is the next step? | The next goal is to assemble all the 4 set of thruster arm equipped with the servo and motor as well. We have to see the vibration level of the thruster arm once it fully loaded. |
