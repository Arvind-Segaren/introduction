## LOGBOOK WEEK 10 (31/12/2021)

## Logbook Entry Number : 08

## Group : Team 1

## Subsystem : Design and Structure

| Week 10 | Subsystem |
|--------|-----------|
| 1. What is the agenda this week and what are my/our goals? | For this week, I supposed to cross-check the dimension of newly printed parts with the brand new motor and servo. Unfortunately, there are number of students who tested positive for COVID-19 virus were attending to the laboratory for the past fews days. So, the Faculty has ordered the laboratory to be shutdown for one week due to the sanitation process.|
| 2. What decisions did you/your team make in solving a problem? | During this shutdown period, we couldn't able to proceed our project progress to the next level. Our agenda for this week has been suspended. Thus, we decided to focus on paperwork for our thruster arm design group. |
| 3. How did you/your team make those decisions (method)? | We split the report work among ourselves. Each and everyone has their own contribution to the report task. |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Since the laboratory shutdown for a week, we couldn't do anything physically. So, we had plenty of time to start the report progress. |
| 5. What was the impact on you/your team/the project when you make that decision? | Atleast we did something during this period where the laboratory has been shutdown for a week. |
| 6. What is the next step? | Our next move is to complete this week actual agenda in the upcoming week. |
