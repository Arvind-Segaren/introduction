## LOGBOOK WEEK 14 (28/1/2022)

## Logbook Entry Number : 12

## Group : Team 1

## Subsystem : Design and Structure

| Week 14 | Subsystem |
|--------|-----------|
| 1. What is the agenda this week and what are my/our goals? | This week Friday marks as D-day for our IDP project because we are about to fly the airship on this day. The preliminary work has been started at 7am in the Friday morning. The FSI team has started to inflate the airship with the helium gas. The Thruster arm team has been working on the installation of the arm with the airship. The Control team has been setting-up their work station to tune the airship with their controller. Everything went smooth till we fly the airship!. The airship lifted-off at 10:30am with four side rope on it as a balancer. The rope controlled by four students and they will align the airship manually if anything goes wrong. The airship was in the sky for about 50 minutes. A DJI mini drone owned by AEROS club has video recorded the airship in the sky. Then, an accident occured where it caused all of us to become stunt for a moment!. As the wind become stronger, the airship started to roll to a side. The pilot has tried to maneuver the airship in the opposite direction to counter-act the roll motion. Unfortunately, that action doesn't work perhaps it added more roll angle and makes the airship to across the stall angle. when more rolling happened, the thruster arm carbon rod couldn't withstand the high tension and force to break one of the carbon rod. After it break, the main rod of the thruster arm loosed it's stability and directed towards the airship and the propeller punctured the airship about 12cm. The footage of this incident has been recorded. The airship started to drop lift level since the helium gas is getting out from the punctured hole. We recovered the airship in the ground safely and brought it back to the laboratory H2.1. The post-mortem meeting was held at 3pm on the same day. |
| 2. What decisions did you/your team make in solving a problem? | 1st, why does the counter-act of the rolling motion didn't worked at that time? Because, the rotation of the propeller was setted in the same direction. Thus, instead of recovering the roll motion, it added more roll angle to the airship. 2nd, why does the carbon rod ruptured and came out which cause the main rod to become unstable? Because, the carbon rod placed too intently with the velcro strap. Therefore, when the rolling happened the carbod rod couldn't withstand the tension and ruptured. |
| 3. How did you/your team make those decisions (method)? | During the post-mortem meeting, the root cause of this incident has been discussed among the students and the supervisors as well.  |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Because an incident occurs in the 1st fly attempt it's normal. The more failure happens the more stronger we get in term of fundamentally and practically. Actually, we didn't fail but we have found out that things won't happened in that way. A post-mortem meeting is a must after a tragedy. So that, everyone knows what is the root cause and what are the corrective action needs to be ammend. |
| 5. What was the impact on you/your team/the project when you make that decision? | Dr.Salahuddin has praised us for making the airship to flew in the sky today. He also congrats us to successfully completed this high-stakes project. The overall satisfactory level has been achieved but improvement can be done as well.  |
| 6. What is the next step? | According to Dr.Salahuddin, the upcoming batch will improvise the elements which we were lacking in this project. |

![This is an image](photo_2022-02-01_13-05-24.jpg) 
![This is an image](photo_2022-02-01_13-05-32.jpg) 
