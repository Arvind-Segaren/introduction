## LOGBOOK WEEK 11 (7/1/2022)

## Logbook Entry Number : 09

## Group : Team 1

## Subsystem : Design and Structure

| Week 11 | Subsystem |
|--------|-----------|
| 1. What is the agenda this week and what are my/our goals? | For this week, we need to cross-check the dimension of the printed parts with the brand new motor and servo. If there is any mismatch dimension error happens, then I will alter the CAD drawing and re-print again.|
| 2. What decisions did you/your team make in solving a problem? | For the motor holder, the dimension fitted perfectly but the printed part slightly bended due to the weight of the motor. For servo holder, the thruster arm rod fitted perfectly to the printed hollow cylinder but the dimension of the servo mount was not so accurate. |
| 3. How did you/your team make those decisions (method)? | So, I decided to modify the CAD file and print again the parts for next inspection. For motor holder, the thickness of the printed part need to be increase in order to hold the motor without bending. |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Because printing again is the only choice available. We can't directly modify the printed parts as it will affect the effectiveness and strength of the part. Before printing, the CAD file need to be altered. |
| 5. What was the impact on you/your team/the project when you make that decision? | The CAD file for motor holder has been revised and printed again. The outcome was superb and the printed part can hold the motor without showing any bending symptoms. The CAD file for motor holder part has been fully completed and we can start to print another 3 pieces on next week. |
| 6. What is the next step? | For servo holder part, the CAD file haven't update yet. We will revise the CAD file and print again in the upcoming week. |
