## LOGBOOK WEEK 13 (21/1/2022)

## Logbook Entry Number : 11

## Group : Team 1

## Subsystem : Design and Structure

| Week 13 | Subsystem |
|--------|-----------|
| 1. What is the agenda this week and what are my/our goals? | As we planned earlier, for this week we hand-over our printed thruster arm components to Flight System Integration (FSI) team to do the calibration setup and test-run. Meanwhile, our thruster arm members were gathered and inflate the blimp to do the trial setting for the thruster arm components. After the test-run, FSI team member has experienced vibration and friction noise in the motor holder while the motor running at high speed.|
| 2. What decisions did you/your team make in solving a problem? | To find the root cause of this problem, I requested FSI to do test-run all the four motors to conclude the scenario. Unfortunately, all the motors are producing same kind of issues. I found that, the head shaft of the motor was placed too near to the 3D printed motor holder hole. Thus, I decided to add spacer (washer) to the upper part of the motor and screw it.|
| 3. How did you/your team make those decisions (method)? | I discussed this issue with Previndran and finalised to add spacer to the motor before attach the motor to the motor holder. |
| 4. Why did you/your team make that choice (justification) when solving the problem? | Why add spacer to solve this issue? because by adding spacer, the gap between motor and motor holder will increase. Thus, the motor head shaft will have more room to spin without producing friction noise with the hole wall of the motor holder. |
| 5. What was the impact on you/your team/the project when you make that decision? | The vibration and friction noise were eliminated after added the spacer. |
| 6. What is the next step? | Next, the FSI team will do the calibration process for the servo and motor. |
