<img src = "https://gitlab.com/Arvind-Segaren/introduction/-/raw/main/photo_2021-10-29_15-13-32.jpg" width=150 align=middle>


Good day everyone, I'm Arvind A/L Segaren and I am currently enrolling final year of Bachelor in Aerospace Engineering.


Basically I'm a **deep thinker** and I will think twice before commits to any decision. So, there is a pro and cons situation for my style. The advantage part is I will think wisely before say anything or taking any decision. The down part is I need more time to complete a task because I will make sure the decision I am taking won't be going wrong in the end. 

Strength | Weakness
-------- | --------
Kind | amature programming skill
Wise thinker | Need more time to complete a task
Eager to learn | Homesick

#MY PASSION:

* Gym enthusiast
* Family oriented
* Cars and superbikes



